package apps.startup.myapplication;

import android.widget.EditText;

import apps.startup.myapplication.Utils.BasicUtils;

/**
 * Created by prateek on 14/11/17.
 */

public class CommonFunction {

    public static boolean validate(EditText[] fields){
        for(int i = 0; i < fields.length; i++){
            EditText currentField = fields[i];
            if(currentField.getText().toString().length() <= 0){
                currentField.setError("Cannot Be Empty");
                currentField.requestFocus();
                return false;
            } else if(currentField.getId() == R.id.phone_no) {
                if(!BasicUtils.isValidMobile(currentField.getText().toString())) {
                    currentField.setError("Please enter a valid mobile number");
                    currentField.requestFocus();
                    return false;
                }
            }
        }
        return true;
    }
    public static boolean validateEdit(EditText fields){
            EditText currentField = fields;
            if(currentField.getText().toString().length() <= 0){
                currentField.setError("Cannot Be Empty");
                currentField.requestFocus();
                return false;
            }
        return true;
    }
    private static void setData(EditText editText) {
        editText.setEnabled(false);
        editText.clearFocus();
    }
    public static void setEditText(EditText imei, String deviceId) {
        imei.setText(deviceId);
        setData(imei);
    }
}
