package apps.startup.myapplication;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.stats.ConnectionTracker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.NavigationUI;
import apps.startup.myapplication.Notifications.NotificationBroadcastReceiver;
import apps.startup.myapplication.application.SharedPrefHelper;
import apps.startup.myapplication.base.BaseActivity;
import apps.startup.myapplication.databinding.ActivityLanding2Binding;
import apps.startup.myapplication.model.User;

import static apps.startup.myapplication.Constants.DATABASE_NAME;
import static apps.startup.myapplication.Constants.IS_LOGGED_IN;
import static apps.startup.myapplication.Constants.USER_ID;

public class LandingActivity extends BaseActivity {
    ActivityLanding2Binding binding;
    NavController navController;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startBroadCastReceiver(this,0);
        binding=DataBindingUtil.setContentView(this,R.layout.activity_landing2);
        navController = Navigation.findNavController(LandingActivity.this, R.id.nav_host_fragment);
        NavigationUI.setupWithNavController(binding.navigation,navController);
    }

    private void startBroadCastReceiver(Context context, int i) {
        Intent intent = new Intent(context, NotificationBroadcastReceiver.class);
        intent.putExtra("Order_No",i);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 234324243, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), pendingIntent);
    }

    @Override
    public boolean showBackButton() {
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.user, menu);
        // return true so that the menu pop up is opened
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.log_out) {
            finish();
            FirebaseAuth.getInstance().signOut();
            Intent i=new Intent(this,LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            SharedPrefHelper.getInstance().setBooleanData(IS_LOGGED_IN,false);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }




}
