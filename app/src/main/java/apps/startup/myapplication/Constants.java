package apps.startup.myapplication;

public class Constants {
    public static final String PRINT_PREFERENCES = "shopkeeper_print_preferences";
    public static final String USER_ID = "user_id";
    public static final String USER_NAME="user name";
    public static final String DATABASE_NAME = "shopkeepers";
    public static final String IS_LOGGED_IN = "is_logged_in";
    public static final String LOG_IN_TIME = "log_in_time";
    public static final int REQEST_PERMISSION_CODE=0001;
    public static final String SHOP_DATABASE_NAME="shops";
    public static final String ORDER_DATABASE_NAME="order";
    public static final String USER_DATABASE_NAME="users";
    public static final String TOKEN_PATH="token";
    public static final int BOOKING_AMOUNT=2000;
    public static final String CONTACT_US_FRAGMENT ="Contact Us Fragment";
    public static final String QUERY_DATABASE_NAME="querys";
    public static final String FRAGMENT_NAME="fragment_name";
    public static final String ORDER_DETAIL_FRAGMENT="order_detail_fragment";
    public static final String UNIQUE_ORDER_KEY="time_and_date_of_order";
    public static final String KEY_OF_CUSTORMER="key of customer";
    public static final String APP_URL="https://play.google.com/store/apps/details?id=com.loconav.lookup";
    public static final String PROCESSING="Processing";
    public static final String FILE_PROVIDER_AUTHORITY="com.startup.print";
    public static final String LIMIT_IMAGES="limitImages";
    public static final String IMAGE_LIST="imageList";
    public static final String STARTED_COMPRESSION="started_compression";
    public static final String ACCEPTED="Accepted";
    public static final String SECURITY_DEPOSIT="security deposit";
    public static final String DECLINED="Declined";
    public static final String COMPLETED="Completed";
    public static final String BILL_DATABASE_NAME="bills_photos";
    public static final String HOTEL_DETAIL_UPDATE_FRAGMENT="hotel detail update fragment";
    public static final String HOTEL_DETIALS="hotel details";
    public static final String CUSTOMER_BOOKING_DETAILS ="customer details";
    public static final String PAYMENT_SUCCESS_FRAGMENT="Payment Success Fragment";
}
