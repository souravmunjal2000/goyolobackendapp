package apps.startup.myapplication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import apps.startup.myapplication.Adapter.PreviousOrderListAdapter;
import apps.startup.myapplication.application.SharedPrefHelper;
import apps.startup.myapplication.base.BaseFragment;
import apps.startup.myapplication.databinding.FragmentOrderBinding;
import apps.startup.myapplication.model.CustomerBookingDetails;
import apps.startup.myapplication.model.Order;

import static apps.startup.myapplication.Constants.CUSTOMER_BOOKING_DETAILS;
import static apps.startup.myapplication.Constants.FRAGMENT_NAME;
import static apps.startup.myapplication.Constants.KEY_OF_CUSTORMER;
import static apps.startup.myapplication.Constants.ORDER_DATABASE_NAME;
import static apps.startup.myapplication.Constants.ORDER_DETAIL_FRAGMENT;
import static apps.startup.myapplication.Constants.PAYMENT_SUCCESS_FRAGMENT;
import static apps.startup.myapplication.Constants.UNIQUE_ORDER_KEY;
import static apps.startup.myapplication.Constants.USER_ID;

public class OrderFragment extends BaseFragment {
    FragmentOrderBinding binding;
    PreviousOrderListAdapter previousOrderListAdapter;
    String userid= SharedPrefHelper.getInstance().getStringData(USER_ID);
    ArrayList<CustomerBookingDetails> orders=new ArrayList<>();
    @Override
    public int setViewId() {
        return R.layout.fragment_order;
    }

    @Override
    public void onFragmentCreated() {
    rvInit();
    getData();
    }

    private void getData() {
        DatabaseReference reference= FirebaseDatabase.getInstance().getReference().child(ORDER_DATABASE_NAME);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot d:dataSnapshot.getChildren()) {
                    for (DataSnapshot d1 : d.getChildren()) {
                        binding.error.setVisibility(View.GONE);
                        if(d1.getValue(CustomerBookingDetails.class).getHotelOnwerUID().equals(userid)) {
                            CustomerBookingDetails order=d1.getValue(CustomerBookingDetails.class);
                            orders.add(order);
                        }
                        previousOrderListAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    public void rvInit(){
        previousOrderListAdapter =new PreviousOrderListAdapter(orders,getContext() ,new Callback() {
            @Override
            public void onEventDone(Object object) {
                CustomerBookingDetails customerBookingDetails=(CustomerBookingDetails) object;
                Intent intent=new Intent(getContext(),BaseNavigationActivity.class);
                Bundle bundle=new Bundle();
                bundle.putString(FRAGMENT_NAME,PAYMENT_SUCCESS_FRAGMENT);
                bundle.putSerializable(CUSTOMER_BOOKING_DETAILS,customerBookingDetails);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        binding.rvOrderList.setAdapter(previousOrderListAdapter);
        binding.rvOrderList.setLayoutManager(linearLayoutManager);
    }

    @Override
    public void bindView(View view) {
        binding= DataBindingUtil.bind(view);
    }

    @Override
    public void getComponentFactory() {
    }
}
