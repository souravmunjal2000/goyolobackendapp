package apps.startup.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import apps.startup.myapplication.base.BaseActivity;

import static apps.startup.myapplication.Constants.CONTACT_US_FRAGMENT;
import static apps.startup.myapplication.Constants.FRAGMENT_NAME;
import static apps.startup.myapplication.Constants.HOTEL_DETAIL_UPDATE_FRAGMENT;
import static apps.startup.myapplication.Constants.ORDER_DETAIL_FRAGMENT;
import static apps.startup.myapplication.Constants.PAYMENT_SUCCESS_FRAGMENT;

public class BaseNavigationActivity extends BaseActivity {
    FragmentController fragmentController=new FragmentController();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_navigation);
        String fragment_name=getIntent().getExtras().getString(FRAGMENT_NAME);
        switch (fragment_name)
        {
            case HOTEL_DETAIL_UPDATE_FRAGMENT:
                HotelDetailUpdateFragment hotelDetailUpdateFragment=new HotelDetailUpdateFragment();
                fragmentController.loadFragment(hotelDetailUpdateFragment,getSupportFragmentManager(),R.id.base_navigation_host,false);
                break;
            case CONTACT_US_FRAGMENT:
                ContactUsFragment contactUsFragment =new ContactUsFragment();
                fragmentController.loadFragment(contactUsFragment,getSupportFragmentManager(),R.id.base_navigation_host,false);
                break;
            case PAYMENT_SUCCESS_FRAGMENT:
                PaymentPaidSuccessFullFragment paymentPaidSuccessFullFragment=new PaymentPaidSuccessFullFragment();
                fragmentController.loadFragment(paymentPaidSuccessFullFragment,getSupportFragmentManager(),R.id.base_navigation_host,false);
                break;
        }
    }

    @Override
    public boolean showBackButton() {
        return true;
    }
}
