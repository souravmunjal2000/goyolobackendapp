package apps.startup.myapplication;

import android.app.Activity;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Toast;


import apps.startup.myapplication.application.PrintApplication;

public class Toaster {
    public static void makeToast(String message)
    {
        Toast.makeText(PrintApplication.getInstance().getBaseContext(), ""+message, Toast.LENGTH_SHORT).show();
    }
    public static void makeSnackBarFromActivity(Activity activity, String message) {
        if(activity!=null) {
            final Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG);
            snackbar.setAction("Dismiss", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    snackbar.dismiss();
                }
            });
            snackbar.show();
        }
    }

    public static void makeSnackBarFromView(View view, String message) {
        if(view!=null) {
            final Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
            snackbar.setAction("Dismiss", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    snackbar.dismiss();
                }
            });
            snackbar.show();
        }
    }
}
