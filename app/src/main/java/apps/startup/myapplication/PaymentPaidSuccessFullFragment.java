package apps.startup.myapplication;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.Locale;

import apps.startup.myapplication.Adapter.TotalBillItemsAdapter;
import apps.startup.myapplication.base.BaseFragment;
import apps.startup.myapplication.databinding.FragmentPaymentPaidSuccessBinding;
import apps.startup.myapplication.model.AddShops;
import apps.startup.myapplication.model.CustomerBookingDetails;
import apps.startup.myapplication.model.TotalBookedItems;

import static apps.startup.myapplication.Constants.BOOKING_AMOUNT;
import static apps.startup.myapplication.Constants.CUSTOMER_BOOKING_DETAILS;
import static apps.startup.myapplication.Constants.SECURITY_DEPOSIT;
import static apps.startup.myapplication.Constants.SHOP_DATABASE_NAME;


public class PaymentPaidSuccessFullFragment extends BaseFragment {
    FragmentPaymentPaidSuccessBinding binding;
    CustomerBookingDetails customerBookingDetails;
    @Override
    public int setViewId() {
        return R.layout.fragment_payment_paid_success;
    }

    @Override
    public void onFragmentCreated() {
        customerBookingDetails=(CustomerBookingDetails) getActivity().getIntent().getExtras().getSerializable(CUSTOMER_BOOKING_DETAILS);
        binding.setCustomerDetails(customerBookingDetails);
        setRecyclerView();
        setTotalBill();
        setDiscountLayout();
        setDirectionAndCallDetail();
    }

    private void setDirectionAndCallDetail() {
        DatabaseReference databaseReference= FirebaseDatabase.getInstance().getReference().child(SHOP_DATABASE_NAME).child(customerBookingDetails.getHotelOnwerUID()).child(customerBookingDetails.getHotel());
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                AddShops hotelDetails= dataSnapshot.getValue(AddShops.class);
                binding.direction.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String uri = String.format(Locale.ENGLISH, "geo:%f,%f", hotelDetails.getLat(), hotelDetails.getLongi());
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                        getContext().startActivity(intent);
                    }
                });
                binding.callHotel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String phone = hotelDetails.getPhoneno();
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                        startActivity(intent);
                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    private void setDiscountLayout() {
        if(customerBookingDetails.getDiscount()!=0){
            binding.discountLayout.setVisibility(View.VISIBLE);
            binding.discount.setText(""+customerBookingDetails.getDiscount());
        }
    }

    private void setRecyclerView() {
        TotalBillItemsAdapter totalBillItemsAdapter=new TotalBillItemsAdapter(customerBookingDetails.getTotalBookedItems(),true);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        binding.rvBillItems.setAdapter(totalBillItemsAdapter);
        binding.rvBillItems.setLayoutManager(linearLayoutManager);
    }

    private void setTotalBill(){
        int totalMoney=0;
        for(TotalBookedItems totalBookedItems:customerBookingDetails.getTotalBookedItems()){
            if(!totalBookedItems.getFacilityName().equals(SECURITY_DEPOSIT)){
            totalMoney=totalMoney+(totalBookedItems.getRooms()*(Integer.valueOf(totalBookedItems.getPricesPerRoom()))*totalBookedItems.getDuration())-(BOOKING_AMOUNT*totalBookedItems.getRooms());}
            else
            {
                totalMoney=totalMoney+(totalBookedItems.getRooms()*(Integer.valueOf(totalBookedItems.getPricesPerRoom()))*totalBookedItems.getDuration());
            }
        }
        binding.netRemainingAmount.setText(getString(R.string.ruppee_symbol)+(totalMoney-customerBookingDetails.getDiscount()));
    }

    @Override
    public void bindView(View view) {
        binding= DataBindingUtil.bind(view);
    }

    @Override
    public void getComponentFactory() {

    }
}
