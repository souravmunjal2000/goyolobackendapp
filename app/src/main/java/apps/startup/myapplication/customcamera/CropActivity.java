package apps.startup.myapplication.customcamera;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import apps.startup.myapplication.R;
import apps.startup.myapplication.databinding.ActivityCropBinding;

import static apps.startup.myapplication.customcamera.CameraConstants.CROPPED_IMAGE;
import static apps.startup.myapplication.customcamera.CameraConstants.FILE_PROVIDER_AUTHORITY;


public class CropActivity extends AppCompatActivity {
    ActivityCropBinding binding;
    File croppedFile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this, R.layout.activity_crop);
        String uri=getIntent().getExtras().getString("uri");
        binding.cropImageView.setImageUriAsync(Uri.parse(uri));

        binding.crop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap bitmap=binding.cropImageView.getCroppedImage();
                Intent resultIntent=new Intent();
                try  {
                    croppedFile= ImageUtils.getImagefile();
                    FileOutputStream out = new FileOutputStream(croppedFile);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                resultIntent.putExtra(CROPPED_IMAGE, FileProvider.getUriForFile(getBaseContext(), FILE_PROVIDER_AUTHORITY, croppedFile).toString());
                setResult(RESULT_OK,resultIntent);
                finish();
            }
        });

        binding.nocrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
