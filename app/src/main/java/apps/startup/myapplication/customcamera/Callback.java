package apps.startup.myapplication.customcamera;

/**
 * Created by prateek on 29/05/18.
 */

public interface Callback {
    void onEventDone(Object object);
}
