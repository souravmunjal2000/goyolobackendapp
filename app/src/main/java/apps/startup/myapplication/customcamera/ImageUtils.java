package apps.startup.myapplication.customcamera;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import apps.startup.myapplication.application.PrintApplication;

import static apps.startup.myapplication.customcamera.CameraConstants.FILE_PROVIDER_AUTHORITY;


public class ImageUtils {

    private static final Context FILE_CONTEXT = PrintApplication.getInstance();

    private static ImageUri compressImageFile(ImageUri imageUri) throws IOException {
        File imagefile=getImagefile();
        FileOutputStream fout=new FileOutputStream(imagefile);
        Bitmap bitmap= MediaStore.Images.Media.getBitmap(FILE_CONTEXT.getContentResolver(), imageUri.getUri());
        bitmap= Bitmap.createScaledBitmap(bitmap,(bitmap.getWidth()*30)/100,(bitmap.getHeight()*30)/100,true);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 30, fout);
        ImageUri compressedImageUri=new ImageUri();
        compressedImageUri.setUri(FileProvider.getUriForFile(FILE_CONTEXT,FILE_PROVIDER_AUTHORITY, imagefile));
        return compressedImageUri;
    }

    public static ArrayList<ImageUri> compressImageList(ArrayList<ImageUri> imageUriArrayList) throws IOException {
        ArrayList<ImageUri> newImageUriList=new ArrayList<>();
        for(ImageUri imageUri :imageUriArrayList)
        {
            newImageUriList.add(compressImageFile(imageUri));
        }
        return newImageUriList;
    }

    public static File getImagefile() throws IOException {
        File storageDir;
        String imageFileName = "JPEG_" + "Loconav" + "_";
        storageDir = FILE_CONTEXT.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );
    }

    public static Bitmap getThumbnailImage(Uri image, Context context) throws IOException
    {
        final int THUMBSIZE = 256;//pixels
        Log.e("the image is ","the big image path is "+image.getPath());
        return  ThumbnailUtils.extractThumbnail(BitmapFactory.decodeStream(context.getContentResolver().openInputStream(image)),
                THUMBSIZE, THUMBSIZE);
    }

//    public static String getbase64Image(Bitmap bitmap)
//    {
//        String str= "data:image/png;base64,"+encodeToBase64(bitmap, Bitmap.CompressFormat.JPEG,100);
//        return str;
//    }


}
