package apps.startup.myapplication.customcamera;

import android.Manifest;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.Objects;

import apps.startup.myapplication.FragmentController;
import apps.startup.myapplication.R;
import apps.startup.myapplication.Utils.PermissionUtils;
import apps.startup.myapplication.base.BaseActivity;
import apps.startup.myapplication.databinding.ActivityCameraOpenBinding;

import static apps.startup.myapplication.customcamera.CameraConstants.REQEST_PERMISSION_CODE;

public class CameraOpenActivity extends BaseActivity {
    private ActivityCameraOpenBinding cameraOpenBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cameraOpenBinding= DataBindingUtil.setContentView(this, R.layout.activity_camera_open);
        boolean CameraPermission= PermissionUtils.checkPermission(this, Manifest.permission.CAMERA);
        if(!CameraPermission)
        {
            PermissionUtils.askPermission(this,new String[]{Manifest.permission.CAMERA});
        }
        else {
            showCamera();
        }
    }

    @Override
    public boolean showBackButton() {
        return false;
    }

    public void showCamera(){
        FragmentController fragmentController=new FragmentController();
        Bundle bundle=new Bundle();
        bundle.putInt("limit", Objects.requireNonNull(getIntent().getExtras()).getInt("limit"));
        bundle.putString("Stringid",getIntent().getExtras().getString("Stringid"));
        CameraPickerFragment cameraPickerFragment=new CameraPickerFragment();
        cameraPickerFragment.setArguments(bundle);
        fragmentController.loadFragment(cameraPickerFragment,getSupportFragmentManager(), R.id.camera,false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cameraOpenBinding.unbind();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == REQEST_PERMISSION_CODE) {
            boolean allPermissionsGranted = true;
            Log.e("tab",""+permissions.length+grantResults.length);
            for(int result : grantResults) {
                if(result != 0) {
                    allPermissionsGranted = false;
                }
            }

            if(!allPermissionsGranted) {
                onBackPressed();
            }else{
                showCamera();
            }
        }
    }
}
