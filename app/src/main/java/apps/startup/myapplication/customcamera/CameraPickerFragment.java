package apps.startup.myapplication.customcamera;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import apps.startup.myapplication.Adapter.RecycleCustomImageAdapter;
import apps.startup.myapplication.R;
import apps.startup.myapplication.Toaster;
import apps.startup.myapplication.Utils.TutorialUtils;
import apps.startup.myapplication.base.BaseFragment;
import apps.startup.myapplication.databinding.FragmentCamerapickerBinding;

import static android.app.Activity.RESULT_OK;
import static apps.startup.myapplication.customcamera.CameraConstants.CROPPED_IMAGE;
import static apps.startup.myapplication.customcamera.CameraConstants.IMAGE_LIST;
import static apps.startup.myapplication.customcamera.CameraConstants.LIMIT;
import static apps.startup.myapplication.customcamera.CameraConstants.NO_TUTORIAL;
import static apps.startup.myapplication.customcamera.CameraConstants.REQUEST_CODE_FOR_CROP;
import static apps.startup.myapplication.customcamera.CameraConstants.TUTORIAL_NAME;


public class CameraPickerFragment extends BaseFragment {
    private FragmentCamerapickerBinding binding;
    private Camera mCamera;
    private Camera.Parameters parameters;
    private boolean isFlashOn=false;
    private final ArrayList<ImageUri> imageList=new ArrayList<>();
    private RecycleCustomImageAdapter recycleCustomImageAdapter;
    private boolean safeToTakePhoto=false;//this checks if the surface is created or not and user is allowed to take photos and autofocus
    private int limit;
    private String tutorialName;
    @Override
    public int setViewId() {
        return R.layout.fragment_camerapicker;
    }

    @Override
    public void onFragmentCreated() {
        limit= getActivity().getIntent().getExtras().getInt(LIMIT);
        tutorialName=getActivity().getIntent().getExtras().getString(TUTORIAL_NAME);


        // Create an instance of Camera
        mCamera = getCameraInstance();

        // Create our Preview view and set it as the content of our activity.
        CameraPreview mPreview = new CameraPreview(getContext(), mCamera);
        FrameLayout preview = binding.cameraPreview;
        preview.addView(mPreview);

        //set recyclerview adapter
        setImageAdapter();



        //Capture the photo and save it
        binding.capture.setOnClickListener(view -> {
            binding.capture.setClickable(false);
            if(imageList.size()<limit && safeToTakePhoto) {
                mCamera.takePicture(null, null, (bytes, camera) -> {
                    File pictureFile = null;
                    try {
                        pictureFile = ImageUtils.getImagefile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (pictureFile == null) {
                        Log.d("hey", "Error creating media file, check storage permissions");
                        return;
                    }

                    try {
                        FileOutputStream fos = new FileOutputStream(pictureFile);
                        fos.write(bytes);
                        fos.close();
                        mCamera.startPreview();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    Intent intent=new Intent(getActivity(), CropActivity.class);
                    Bundle bundle=new Bundle();
                    bundle.putString("uri", String.valueOf(FileProvider.getUriForFile(getContext(), CameraConstants.FILE_PROVIDER_AUTHORITY, pictureFile)));
                    intent.putExtras(bundle);
                    startActivityForResult(intent,REQUEST_CODE_FOR_CROP);
                });
            }
            else
            {
                Toaster.makeSnackBarFromActivity(getActivity(),getString(R.string.size_limit)+limit);
            }
        });

        //It is for auto focus when user touches the screen and it is only enable for rear camera
        binding.cameraPreview.setOnTouchListener((v, event) -> {
            if(mCamera!=null && mCamera.getParameters()!=null) {
                if (event.getAction() == MotionEvent.ACTION_DOWN && safeToTakePhoto) {
                    mCamera.autoFocus((b, camera) -> {
                    });
                }
            }
            return true;
        });

        //for turn ON or OFF the Flash in camera
        binding.flash.setOnClickListener(v -> {
            if(mCamera!=null) {
                parameters = mCamera.getParameters();
                if (parameters!=null &&parameters.getSupportedFlashModes() != null) {
                    if (isFlashOn) {
                        isFlashOn = false;
                        binding.flash.setImageResource(R.drawable.noflash);
                        parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                    } else {
                        isFlashOn = true;
                        binding.flash.setImageResource(R.drawable.flash);
                        parameters.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
                    }
                    mCamera.setParameters(parameters);
                }
            }
        });

        //When the Images are Final
        binding.totalcorrect.setOnClickListener(view -> {
            Intent returnIntent = new Intent();
            //As Uri is not parceble/Seriazable we have to convert it into string list
            ArrayList<String> imageListinString=new ArrayList<>();
            for(ImageUri uri:imageList)
                imageListinString.add(uri.getUri().toString());
            returnIntent.putExtra(IMAGE_LIST,imageListinString);
            getActivity().setResult(RESULT_OK,returnIntent);
            getActivity().finish();
        });

        safeToTakePhoto=true;
    }

    private void setImageAdapter()
    {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recycleCustomImageAdapter=new RecycleCustomImageAdapter(imageList, new Callback() {
            @Override
            public void onEventDone(Object object) {
                ImageUri uri=(ImageUri) object;
                FullImageDialog fullImageDialog = FullImageDialog.newInstance(uri.getUri().toString());
                fullImageDialog.show(getActivity().getSupportFragmentManager(),getClass().getSimpleName());
            }
        },getContext());
        binding.rvImages.setLayoutManager(linearLayoutManager);
        binding.rvImages.setAdapter(recycleCustomImageAdapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Cropped image file uri and update it in recycler view
        if(requestCode==REQUEST_CODE_FOR_CROP && resultCode==RESULT_OK)
        {
            String uri=(String) data.getExtras().get(CROPPED_IMAGE);
            ImageUri imageUri = new ImageUri();
            imageUri.setUri(Uri.parse(uri));
            imageList.add(imageUri);
            recycleCustomImageAdapter.notifyDataSetChanged();
            //This is to show tutorial for further images once the images are clicked
            showTutorial();
        }
        binding.capture.setClickable(true);
    }

    private void showTutorial() {
        if(!tutorialName.equals(NO_TUTORIAL) && imageList.size()<limit) {
            TutorialUtils.showTutorial(tutorialName,getActivity().getSupportFragmentManager());
        }
    }

    private Camera getCameraInstance(){
        Camera camera = null;
        try {
            camera = Camera.open();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return camera;
    }

    @Override
    public void bindView(View view) {
        binding= DataBindingUtil.bind(view);
    }

    @Override
    public void getComponentFactory() {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding.unbind();
        if(mCamera!=null) {
            mCamera.stopPreview();
            mCamera.release();
        }
    }

}
