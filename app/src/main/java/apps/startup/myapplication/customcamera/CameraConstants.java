package apps.startup.myapplication.customcamera;

public class CameraConstants {
    public static final String LIMIT="limit";
    public static final String TUTORIAL_NAME="tutorial name";
    public static final String FILE_PROVIDER_AUTHORITY="com.startup.print";
    public static final int REQUEST_CODE_FOR_CROP=03;
    public static final String IMAGE_LIST="imageList";
    public static final String CROPPED_IMAGE="cropped_image";
    public static final String NO_TUTORIAL="no tutorial";
    public static final String STARTED_COMPRESSION="started_compression";
    public static final String ID = "id";
    public static final String LIMIT_IMAGES="limitImages";
    public static final int REQEST_PERMISSION_CODE=0001;
}
