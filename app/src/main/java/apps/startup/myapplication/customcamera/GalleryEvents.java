package apps.startup.myapplication.customcamera;


import apps.startup.myapplication.base.PubSubEvent;


class GalleryEvents extends PubSubEvent {

    public GalleryEvents(String message, Object object) {
        super(message, object);
    }

    public GalleryEvents(String message) {
        super(message);
    }

    public static  final String IMAGE_COMPRESSED = "image_compressed";

}
