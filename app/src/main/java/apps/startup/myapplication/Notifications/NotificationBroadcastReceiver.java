package apps.startup.myapplication.Notifications;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import apps.startup.myapplication.LandingActivity;
import apps.startup.myapplication.R;
import apps.startup.myapplication.application.SharedPrefHelper;
import apps.startup.myapplication.model.OrderNo;

import static android.content.Context.ALARM_SERVICE;
import static apps.startup.myapplication.Constants.TOKEN_PATH;
import static apps.startup.myapplication.Constants.USER_ID;

public class NotificationBroadcastReceiver extends BroadcastReceiver {
    OrderNo i=new OrderNo();
    @Override
    public void onReceive(Context context, Intent intent) {
        DatabaseReference firebaseDatabase= FirebaseDatabase.getInstance().getReference(TOKEN_PATH).child(SharedPrefHelper.getInstance().getStringData(USER_ID));
        firebaseDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                i = dataSnapshot.getValue(OrderNo.class);
                if(i!=null && i.isNewOrder() ) {
                    i.setNewOrder(false);
                    firebaseDatabase.setValue(i);
                    sendNotification(context,i.getOrderno());
                }
                startBroadCastReceiver(context);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                startBroadCastReceiver(context);
            }
        });
        startBroadCastReceiver(context);
    }

    private void startBroadCastReceiver(Context context) {
        Intent intent = new Intent(context, NotificationBroadcastReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 234324243, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), pendingIntent);
    }


    private  void sendNotification(Context context,int i) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        String CHANNEL_ID = "my_channel_01";
        CharSequence name = "my_channel";
        int importance = NotificationManager.IMPORTANCE_HIGH;
        NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
        notificationManager.createNotificationChannel(mChannel);

        // Create an Intent for the activity you want to start
        Intent resultIntent = new Intent(context, LandingActivity.class);
        // Create the TaskStackBuilder and add the intent, which inflates the back stack
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntentWithParentStack(resultIntent);
        // Get the PendingIntent containing the entire back stack
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle("New Order")
                .setContentText("Heads Up!!You got a new order")
                .setSmallIcon(R.drawable.icon)
                .setColor(context.getResources().getColor(R.color.printbasicblue))
                .setSound(defaultSound)
                .setContentIntent(resultPendingIntent);
        // notificationId is a unique int for each notification that you must define
        notificationManager.notify(i, mBuilder.build());
    }

}
