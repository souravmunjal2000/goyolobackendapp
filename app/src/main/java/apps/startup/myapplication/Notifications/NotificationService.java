package apps.startup.myapplication.Notifications;

import android.app.IntentService;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Timer;
import java.util.TimerTask;

import apps.startup.myapplication.LandingActivity;
import apps.startup.myapplication.R;
import apps.startup.myapplication.application.SharedPrefHelper;
import apps.startup.myapplication.model.Order;
import apps.startup.myapplication.model.OrderNo;

import static apps.startup.myapplication.Constants.TOKEN_PATH;
import static apps.startup.myapplication.Constants.USER_ID;

public class NotificationService extends Service {

    Context context;
    public boolean first=false;
    public NotificationService(Context applicationContext)
    {
        context=applicationContext;
    }

    public NotificationService()
    {

    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        first=false;
        ss();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        ss();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.i("EXIT", "ondestroy!");

        Intent broadcastIntent = new Intent("ac.in.ActivityRecognition.RestartSensor");
        sendBroadcast(broadcastIntent);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }



   public void ss() {
        DatabaseReference firebaseDatabase=FirebaseDatabase.getInstance().getReference(TOKEN_PATH).child(SharedPrefHelper.getInstance().getStringData(USER_ID));
        firebaseDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(first) {
                Log.e("skfd","soruavm munjal is my name so it is important that my father");
                OrderNo i = dataSnapshot.getValue(OrderNo.class);
                if(i!=null) {
                        sendNotification(i.getOrderno());
                }
                }
                first=true;
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private  void sendNotification(int i) {

        // Create an Intent for the activity you want to start
        Intent resultIntent = new Intent(this, LandingActivity.class);
        // Create the TaskStackBuilder and add the intent, which inflates the back stack
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(resultIntent);
        // Get the PendingIntent containing the entire back stack
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, "sd")
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle("New Order")
                .setContentText("Heads Up!!You got a new order")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setSmallIcon(R.drawable.icon)
                .setColor(getResources().getColor(R.color.printbasicblue))
                .setSound(defaultSound)
                .setContentIntent(resultPendingIntent);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

        // notificationId is a unique int for each notification that you must define
        notificationManager.notify(i, mBuilder.build());
    }



}