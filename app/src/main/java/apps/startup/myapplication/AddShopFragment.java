package apps.startup.myapplication;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;

import apps.startup.myapplication.application.SharedPrefHelper;
import apps.startup.myapplication.base.BaseFragment;
import apps.startup.myapplication.customcamera.CustomImagePicker;
import apps.startup.myapplication.customcamera.ImageUri;
import apps.startup.myapplication.databinding.FragmentAddShopsBinding;
import apps.startup.myapplication.model.AddShops;
import apps.startup.myapplication.model.Facilitiesandprices;
import apps.startup.myapplication.model.Input;

import static apps.startup.myapplication.Constants.ORDER_DATABASE_NAME;
import static apps.startup.myapplication.Constants.SHOP_DATABASE_NAME;
import static apps.startup.myapplication.Constants.USER_ID;

public class AddShopFragment extends BaseFragment {
    FragmentAddShopsBinding binding;
    String shopname, phoneno, address;
    int numberOfLines = 0, facilities = 0, price = 0,noofrooms=0;
    ArrayList<Facilitiesandprices> facilities_list = new ArrayList<>();
    ArrayList<ImageUri> imageUrisList = new ArrayList<>();
    CustomImagePicker imagePicker;
    private DatabaseReference mDatabase;
    UploadTask uploadTask;
    ProgressDialog progressDialog;
    ArrayList<String> generalImages;
    String whatshopdo = "";
    String url;
    String description;
    final AddShops shops = new AddShops();
    ArrayList<String> facilitiesList=new ArrayList<>();
    String user_id=SharedPrefHelper.getInstance().getStringData(USER_ID);


    @Override
    public int setViewId() {
        return R.layout.fragment_add_shops;
    }

    @Override
    public void onFragmentCreated() {
        setLocation();
        progressDialog = new ProgressDialog(getContext());
        Add_Line();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        binding.addFacilities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Add_Line();
            }
        });
        progressDialog.setMessage("Uploading....");
        progressDialog.setCancelable(false);
        binding.submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CommonFunction.validate(new EditText[]{binding.securityDeposit,binding.shopDescription,binding.shopName, binding.shopAddressLine1, binding.shopAddressLine2, binding.shopAddressLine3, binding.phoneNo,binding.cleaningStaffNo1,binding.cleaningStaffNo2,binding.guardNo}) && checkImages(binding.ShopImage, "Shop Images")) {
                    shopname = binding.shopName.getText().toString();
                    phoneno = "+91" + binding.phoneNo.getText().toString();
                    address = binding.shopAddressLine1.getText().toString() + " " + binding.shopAddressLine2.getText().toString() + " " + binding.shopAddressLine3.getText().toString();
                    description=binding.shopDescription.getText().toString();


                    getGeneralPhotos();
                    progressDialog.show();
                    Log.e("ss", "the facilities are" + facilities_list.toString());
                }
            }
        });
    }

    private void getGeneralPhotos() {
        getImageUrls(binding.GeneralImage.getimagesList(), new Callback() {
            @Override
            public void onEventDone(Object object) {
                generalImages=(ArrayList<String>)object;
                getFacilitiesandPrice();
            }
        },"GeneralPhotos");
    }

    private void setLocation() {
        LocationManager locManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000L, 500.0f, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
               double latitude = location.getLatitude();
               double longitude = location.getLongitude();
               binding.latitude.setText(""+latitude);
               binding.longitutde.setText(""+longitude);
               shops.setLat(latitude);
               shops.setLongi(longitude);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        });
    }


    public void getImageUrls(ArrayList<ImageUri> attachments,Callback callback,String foldername)
    {
        ArrayList<String> url=new ArrayList<>();
        StorageReference storageReference=FirebaseStorage.getInstance().getReference().child(SHOP_DATABASE_NAME).child(user_id).child(shopname).child(foldername);
        for(int i =0;i<attachments.size();i++) {
            progressDialog.show();
            final StorageReference temp=storageReference.child("image_"+(i+1));
            uploadTask=temp.putFile(attachments.get(i).getUri());
            final int finalI = i;
            Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        progressDialog.dismiss();
                        throw task.getException();
                    }
                    // Continue with the task to get the download URL
                    return temp.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        url.add(task.getResult().toString()) ;
                        if(finalI==attachments.size()-1)
                            callback.onEventDone(url);
                    } else {
                        progressDialog.dismiss();
                        // Handle failures
                        // ...
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), ""+e.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }

    }


    public void uploadimage()
    {
        StorageReference storageReference=FirebaseStorage.getInstance().getReference().child(SHOP_DATABASE_NAME).child(user_id).child(shopname);
        for(int i =0;i<imageUrisList.size();i++) {
            final StorageReference temp=storageReference.child("image_"+(i+1));
            uploadTask=temp.putFile(imageUrisList.get(i).getUri());
            Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }
                    // Continue with the task to get the download URL
                    return temp.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        url = task.getResult().toString();
                        uploadToDatabase();
                    } else {
                        // Handle failures
                        // ...
                    }
                }
            });
        }
    }

    public boolean checkImages(CustomImagePicker imagePicker, String title)
    {
        if(!imagePicker.getimagesList().isEmpty()) {
            return true;
        }
        else
        {
            Toast.makeText(getContext(), "Upload "+title+" Images", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    public void uploadToDatabase()
    {
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                progressDialog.dismiss();
                Toast.makeText(getContext(), ""+exception.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                shops.setUserid(SharedPrefHelper.getInstance().getStringData(USER_ID));
                getFacilitiesList();
                shops.setDescription(description);
                shops.setFacilities(facilitiesList);
                shops.setPhotourl(url);
                shops.setName(shopname);
                shops.setBasisOfPayment(getSelectedRadioButton(binding.basisType));
                shops.setSecurityDeposit(Integer.parseInt(binding.securityDeposit.getText().toString()));
                shops.setPhoneno(phoneno);
                shops.setCleaningStaffPhoneno1(binding.cleaningStaffNo1.getText().toString());
                shops.setCleaningStaffPhoneno2(binding.cleaningStaffNo2.getText().toString());
                shops.setSecurityGuardPhoneno(binding.guardNo.getText().toString());
                shops.setAddress(address);
                shops.setTypeOfPG(getSelectedRadioButton(binding.radioGroupPGType));
                shops.setTypeOfFood(getSelectedRadioButton(binding.radioGroupFoodType));
                shops.setRating(5);
                shops.setWhatshopsdo(whatshopdo);
                shops.setWhat_shops_do(facilities_list);
                shops.setAffiliatedCollege(binding.collegeAffiliatedTo.getText().toString());
                shops.setGeneralPhotoList(generalImages);
                mDatabase.child(SHOP_DATABASE_NAME).child(user_id).child(shopname).setValue(shops);
                progressDialog.dismiss();
                final android.support.v7.app.AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.DialogTheme);
                builder.setMessage("New Shop created successfully")
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent=new Intent(getActivity(),LandingActivity.class);
                                startActivity(intent);
                            }
                        })
                        .setCancelable(false)
                        .show();
            }
        });
    }

    private String getSelectedRadioButton(RadioGroup radioGroup) {
        RadioButton radioButton=getActivity().findViewById(radioGroup.getCheckedRadioButtonId());
        return radioButton.getText().toString();
    }

    private void getFacilitiesList() {
        LinearLayout layout=binding.checkboxfacilities;
        int count=layout.getChildCount();
        View v = null;
        for(int i=0;i<count;i++){
            v=layout.getChildAt(i);
            if(v instanceof CheckBox){
                CheckBox checkBox=(CheckBox) v;
                if(checkBox.isChecked()){
                    facilitiesList.add(checkBox.getText().toString());
                }
            }
        }
    }

    public void getFacilitiesandPrice()
    {
        LinearLayout layout=binding.facilitiesandprices;
        int count=layout.getChildCount();
        View v = null;
        for (int i = 0; i < count; i++) {
            v = layout.getChildAt(i);
            if(v instanceof CustomImagePicker){
                CustomImagePicker customImagePicker=(CustomImagePicker) v;
                int finalI = i;
                getImageUrls(customImagePicker.getimagesList(), new Callback() {
                    @Override
                    public void onEventDone(Object object) {
                        Facilitiesandprices facilitiesandprices=new Facilitiesandprices();
                        View view = layout.getChildAt(finalI -1);
                        //Get the prices and the rooms and the prices
                        if(view instanceof LinearLayout) {
                            View ed;
                            int rooms = 0;
                            String facilityname = null,price = null;
                            LinearLayout layout1=(LinearLayout) view;
                            for(int j=0;j<layout1.getChildCount();j++)
                            {
                                ed=layout1.getChildAt(j);
                                if(ed instanceof EditText)
                                {
                                    EditText editText=(EditText)ed;
                                    if(editText.getHint().toString().equals("Facilities")) {
                                        facilitiesandprices.setFacilities(editText.getText().toString());
                                        whatshopdo=whatshopdo+""+editText.getText().toString()+",";
                                    }
                                    if(editText.getHint().toString().equals("Price"))
                                        facilitiesandprices.setPrices(editText.getText().toString());
                                    if(editText.getHint().toString().equals("Rooms"))
                                        facilitiesandprices.setNumberofrooms(Integer.parseInt(editText.getText().toString()));
                                }
                            }
                        }
                        //Set the image list
                        facilitiesandprices.setImages((ArrayList<String>) object);
                        facilities_list.add(facilitiesandprices);
                        if(finalI ==count-1) {
                            getImageUri();
                            uploadimage();
                        }
                    }
                },"facilitiesimages"+i);

            }
        }
    }

    public void getImageUri()
    {
        imagePicker=binding.ShopImage;
        imageUrisList=imagePicker.getimagesList();
    }

    public void Add_Line() {
        LinearLayout ll = binding.facilitiesandprices;
        LinearLayout linearLayout=new LinearLayout(getContext());
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);

        // add edittext
        EditText et1 = new EditText(getContext());
        LinearLayout.LayoutParams p1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,1);
        et1.setLayoutParams(p1);
        et1.setHint("Facilities");
        et1.setId(facilities + 1);
        linearLayout.addView(et1);
        EditText et2 = new EditText(getContext());
        LinearLayout.LayoutParams p2 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,2);
        p2.setMargins(20,0,0,0);
        et2.setLayoutParams(p2);
        et2.setInputType(InputType.TYPE_CLASS_NUMBER);
        et2.setHint("Price");
        et2.setId(price + 1);
        linearLayout.addView(et2);
        EditText et3 = new EditText(getContext());
        LinearLayout.LayoutParams p3 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,2);
        p3.setMargins(20,0,0,0);
        et3.setLayoutParams(p3);
        et3.setInputType(InputType.TYPE_CLASS_NUMBER);
        et3.setHint("Rooms");
        et3.setId(noofrooms + 1);
        linearLayout.addView(et3);

        //add the linear layout in our main linear layout
        ll.addView(linearLayout);
        //Adding the image picker for each facility
        Input input = new Input("facility"+facilities,""+facilities,"photo","facility Photo");
        CustomInflater customInflater =new CustomInflater(getContext());
        customInflater.addImagePicker(ll,input,"Facility Images"+(facilities+1),3,"facilities"+facilities);

        facilities++;
        price++;
        numberOfLines++;
    }

    @Override
    public void bindView(View view) {
        binding= DataBindingUtil.bind(view);
    }

    @Override
    public void getComponentFactory() {

    }
}
