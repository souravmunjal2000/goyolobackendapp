package apps.startup.myapplication;

import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.net.ConnectivityManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;

import apps.startup.myapplication.Notifications.NotificationService;
import apps.startup.myapplication.Notifications.Token;
import apps.startup.myapplication.Utils.BasicUtils;
import apps.startup.myapplication.application.SharedPrefHelper;
import apps.startup.myapplication.base.BaseActivity;
import apps.startup.myapplication.databinding.ActivityLoginBinding;
import apps.startup.myapplication.model.ListOfUsers;
import apps.startup.myapplication.model.User;

import static apps.startup.myapplication.Constants.DATABASE_NAME;
import static apps.startup.myapplication.Constants.IS_LOGGED_IN;
import static apps.startup.myapplication.Constants.TOKEN_PATH;
import static apps.startup.myapplication.Constants.USER_ID;
import static apps.startup.myapplication.Constants.USER_NAME;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LOGIN_ACTIVITY";
    private ActivityLoginBinding activityLoginBinding;
    private String user_id,password;
    DatabaseReference databaseReference;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        databaseReference=FirebaseDatabase.getInstance().getReference().child(DATABASE_NAME);
        activityLoginBinding=DataBindingUtil.setContentView(this,R.layout.activity_login);
        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.verifying));
        progressDialog.setCancelable(false);


        activityLoginBinding.login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                user_id=activityLoginBinding.user.getText().toString();
                password=activityLoginBinding.password.getText().toString();
                Boolean validUser=(user_id.length()==10),validPassword=!(password.length()<6);
                if(!validUser)
                {
                    activityLoginBinding.user.setError(getString(R.string.invalid_email_error));
                }
                if(!validPassword)
                {
                    activityLoginBinding.password.setError(getString(R.string.invalid_password_error));
                }
                if(validPassword && validUser)
                {
                    progressDialog.show();
                    validateUser();
                }
            }
        });
    }

    public void validateUser()
    {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot userSnapshot : dataSnapshot.getChildren()){
                    User user = userSnapshot.getValue(User.class);
                    if(user_id.equals(user.getUser_id())){
                        if (password.equals(user.getPassword())){
                            SharedPrefHelper.getInstance().setBooleanData(IS_LOGGED_IN,true);
                            SharedPrefHelper.getInstance().setStringData(USER_NAME,user.getName());
                            SharedPrefHelper.getInstance().setStringData(USER_ID,user.getUser_id());
                            Intent intent=new Intent(LoginActivity.this,LandingActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }

                    }
                }

                Toaster.makeToast("Invalid Email or password");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }







}
