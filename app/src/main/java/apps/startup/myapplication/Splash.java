package apps.startup.myapplication;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.concurrent.TimeUnit;

import apps.startup.myapplication.Notifications.NotificationService;
import apps.startup.myapplication.application.SharedPrefHelper;
import apps.startup.myapplication.base.BaseActivity;
import apps.startup.myapplication.databinding.ActivitySplashBinding;

import static apps.startup.myapplication.Constants.IS_LOGGED_IN;
import static apps.startup.myapplication.Constants.LOG_IN_TIME;

public class Splash extends BaseActivity {
    ActivitySplashBinding activitySplashBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activitySplashBinding = DataBindingUtil.setContentView(this, R.layout.activity_splash);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                    if(SharedPrefHelper.getInstance().getBooleanData(IS_LOGGED_IN)) {
                        Intent intent = new Intent(Splash.this,LandingActivity.class);
                        startActivity(intent);
                        registerBroadcastReceiver();
                        finish();
                    } else {
                        Intent intent = new Intent(Splash.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    public void registerBroadcastReceiver(){
        NotificationService mSensorService = new NotificationService(getApplicationContext());
        Intent mServiceIntent = new Intent(getApplicationContext(), mSensorService.getClass());
        if (!isMyServiceRunning(mSensorService.getClass())) {
            startService(mServiceIntent);
        }
    }
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.e ("isMyServiceRunning?", true+"");
                return true;
            }
        }
        Log.e ("isMyServiceRunning?", false+"");
        return false;
    }

    @Override
    public boolean showBackButton() {
        return false;
    }
}

