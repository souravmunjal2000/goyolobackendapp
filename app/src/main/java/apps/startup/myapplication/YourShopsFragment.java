package apps.startup.myapplication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import apps.startup.myapplication.Adapter.ShopsAdapter;
import apps.startup.myapplication.application.SharedPrefHelper;
import apps.startup.myapplication.base.BaseFragment;
import apps.startup.myapplication.databinding.FragmentYourShopsBinding;
import apps.startup.myapplication.model.AddShops;

import static apps.startup.myapplication.Constants.FRAGMENT_NAME;
import static apps.startup.myapplication.Constants.HOTEL_DETAIL_UPDATE_FRAGMENT;
import static apps.startup.myapplication.Constants.HOTEL_DETIALS;
import static apps.startup.myapplication.Constants.SHOP_DATABASE_NAME;
import static apps.startup.myapplication.Constants.USER_ID;

public class YourShopsFragment extends BaseFragment {
    FragmentYourShopsBinding binding;
    ShopsAdapter shopsAdapter;
    DatabaseReference firebaseDatabase;
    String userid= SharedPrefHelper.getInstance().getStringData(USER_ID);
    ArrayList<AddShops> addShops=new ArrayList<>();
    ProgressDialog progressDialog;
    @Override
    public int setViewId() {
        return R.layout.fragment_your_shops;
    }

    @Override
    public void onFragmentCreated() {
        progressDialog=new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        binding.addShops.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(),HeadofAddShopActivity.class);
                startActivity(intent);
            }
        });
        firebaseDatabase= FirebaseDatabase.getInstance().getReference().child(SHOP_DATABASE_NAME).child(userid);
        firebaseDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                progressDialog.dismiss();
                for(DataSnapshot d :dataSnapshot.getChildren())
                {
                    addShops.add(d.getValue(AddShops.class));
                    shopsAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        shopsAdapter=new ShopsAdapter(addShops, new Callback() {
            @Override
            public void onEventDone(Object object) {
                AddShops addShops=(AddShops) object;
                Intent intent=new Intent(getContext(),BaseNavigationActivity.class);
                Bundle bundle=new Bundle();
                bundle.putString(FRAGMENT_NAME,HOTEL_DETAIL_UPDATE_FRAGMENT);
                bundle.putSerializable(HOTEL_DETIALS,addShops);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getContext());
        binding.recyclerView.setLayoutManager(linearLayoutManager);
        linearLayoutManager.setOrientation(LinearLayout.VERTICAL);
        binding.recyclerView.setAdapter(shopsAdapter);
    }

    @Override
    public void bindView(View view) {
        binding= DataBindingUtil.bind(view);
    }

    @Override
    public void getComponentFactory() {}
}
