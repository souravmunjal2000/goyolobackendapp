package apps.startup.myapplication.Adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import apps.startup.myapplication.R;
import apps.startup.myapplication.base.BaseAdapter;
import apps.startup.myapplication.model.TotalBookedItems;

public class TotalBillItemsAdapter extends BaseAdapter {
    ArrayList<TotalBookedItems> totalBookedItems=new ArrayList<>();
    boolean showTotalBill;
    public TotalBillItemsAdapter(ArrayList<TotalBookedItems> totalBookedItems, boolean showTotalBill){
        this.totalBookedItems=totalBookedItems;
        this.showTotalBill=showTotalBill;
    }
    @Override
    public Object getDataAtPosition(int position) {
        return totalBookedItems.get(position);
    }

    @Override
    public int getLayoutIdForType(int viewType) {
        return R.layout.total_bill_items;
    }

    @Override
    public void onItemClick(Object object, int position) {

    }

    @Override
    public void editHeightWidthItem(View view, ViewGroup parent) {

    }

    @Override
    public int getItemCount() {
        return totalBookedItems.size();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        if(showTotalBill){
            View view=holder.itemView;
            TextView totalAmountOfRooms=view.findViewById(R.id.total_booking_amount);
            totalAmountOfRooms.setText(""+Integer.valueOf(totalBookedItems.get(position).getPricesPerRoom())*totalBookedItems.get(position).getRooms()*totalBookedItems.get(position).getDuration());
        }
    }
}
