package apps.startup.myapplication.Adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import apps.startup.myapplication.customcamera.Callback;
import apps.startup.myapplication.R;
import apps.startup.myapplication.base.BaseAdapter;
import apps.startup.myapplication.customcamera.ImageUri;


/**
 * Created by sejal on 06-07-2018.
 */

public class RecycleCustomImageAdapter extends BaseAdapter {

    private final ArrayList<ImageUri> data;
    private final Context context;
    private final Callback callback;
    // Provide repair suitable constructor (depends on the kind of dataset)
    public RecycleCustomImageAdapter(ArrayList<ImageUri> myDataset, Callback callback, Context context) {
        data = myDataset;
        this.context=context;
        this.callback = callback;
    }

    @Override
    public Object getDataAtPosition(int position) {
        return data.get(position);
    }


    @Override
    public int getLayoutIdForType(int viewType) {
        return R.layout.gridview_layout;
    }

    @Override
    public void onItemClick(Object object, int position) {
        callback.onEventDone(object);
    }

    @Override
    public void editHeightWidthItem(View view, ViewGroup parent) {}

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);
        View view = holder.itemView;
        ImageView imageView = view.findViewById(R.id.remove);
        imageView.setOnClickListener(view1 -> {
            data.remove(data.get(position));
            notifyDataSetChanged();
        });
    }
}
