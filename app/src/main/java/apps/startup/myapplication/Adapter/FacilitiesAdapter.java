package apps.startup.myapplication.Adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import apps.startup.myapplication.Callback;
import apps.startup.myapplication.R;
import apps.startup.myapplication.base.BaseAdapter;
import apps.startup.myapplication.model.Facilitiesandprices;

public class FacilitiesAdapter extends BaseAdapter {
    public ArrayList<Facilitiesandprices> facilitiesandpricesList;
    public Callback callback;
    public Context context;
    public FacilitiesAdapter(ArrayList<Facilitiesandprices> facilitiesandprices, Callback callback, Context context)
    {
        this.facilitiesandpricesList =facilitiesandprices;
        this.callback=callback;
        this.context=context;
    }


    @Override
    public Object getDataAtPosition(int position) {
        return facilitiesandpricesList.get(position);
    }

    @Override
    public int getLayoutIdForType(int viewType) {
        return R.layout.facilities_items_layout;
    }

    @Override
    public void onItemClick(Object object, int position) {

    }

    @Override
    public void editHeightWidthItem(View view, ViewGroup parent) {

    }

    @Override
    public int getItemCount() {
        return facilitiesandpricesList.size();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        View view=holder.itemView;
        TextView minus=view.findViewById(R.id.minus);
        TextView totalNumber=view.findViewById(R.id.totalnumber);
        TextView plus=view.findViewById(R.id.plus);
        TextView number=view.findViewById(R.id.number);
        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selected= Integer.parseInt(number.getText().toString());
                if(selected!=0){
                    selected--;
                    callback.onEventDone(false);
                    facilitiesandpricesList.get(position).setNumberofrooms(selected);
                    notifyDataSetChanged();
                }
            }
        });
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selected= Integer.parseInt(number.getText().toString());
                selected++;
                callback.onEventDone(true);
                facilitiesandpricesList.get(position).setNumberofrooms(selected);
                notifyDataSetChanged();
            }
        });

    }
}
