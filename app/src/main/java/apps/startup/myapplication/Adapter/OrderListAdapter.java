package apps.startup.myapplication.Adapter;

import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import apps.startup.myapplication.R;
import apps.startup.myapplication.base.BaseAdapter;
import apps.startup.myapplication.model.FacilitiesPricesNumber;


public class OrderListAdapter extends BaseAdapter {
    ArrayList<FacilitiesPricesNumber> facilitiesPricesNumbers;
    public OrderListAdapter(ArrayList<FacilitiesPricesNumber> facilitiesPricesNumbers)
    {
        this.facilitiesPricesNumbers=facilitiesPricesNumbers;
    }
    @Override
    public Object getDataAtPosition(int position) {
        return facilitiesPricesNumbers.get(position);
    }

    @Override
    public int getLayoutIdForType(int viewType) {
        return R.layout.order_items_layout;
    }

    @Override
    public void onItemClick(Object object, int position) {

    }

    @Override
    public void editHeightWidthItem(View view, ViewGroup parent) {

    }

    @Override
    public int getItemCount() {
        return facilitiesPricesNumbers.size();
    }
}
