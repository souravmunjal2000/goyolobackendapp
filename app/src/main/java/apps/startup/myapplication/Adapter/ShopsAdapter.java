package apps.startup.myapplication.Adapter;

import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import apps.startup.myapplication.Callback;
import apps.startup.myapplication.R;
import apps.startup.myapplication.base.BaseAdapter;
import apps.startup.myapplication.model.AddShops;

public class ShopsAdapter extends BaseAdapter {
    public ArrayList<AddShops> shops;
    private Callback callback;

    public ShopsAdapter(ArrayList<AddShops> shops, Callback callback)
    {
        this.shops=shops;
        this.callback=callback;
    }
    @Override
    public Object getDataAtPosition(int position) {
        return shops.get(position);
    }

    @Override
    public int getLayoutIdForType(int viewType) {
        return R.layout.shop_items_layout;
    }

    @Override
    public void onItemClick(Object object, int position) {
        callback.onEventDone(object);
    }

    @Override
    public void editHeightWidthItem(View view, ViewGroup parent) {
    }

    @Override
    public int getItemCount() {
        return shops.size();
    }
}
