package apps.startup.myapplication.Adapter;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import apps.startup.myapplication.BaseNavigationActivity;
import apps.startup.myapplication.Callback;
import apps.startup.myapplication.R;
import apps.startup.myapplication.base.BaseAdapter;
import apps.startup.myapplication.model.CustomerBookingDetails;

import static apps.startup.myapplication.Constants.CONTACT_US_FRAGMENT;
import static apps.startup.myapplication.Constants.FRAGMENT_NAME;

public class PreviousOrderListAdapter extends BaseAdapter {
    ArrayList<CustomerBookingDetails> orders;
    Callback callback;
    Context context;

    public PreviousOrderListAdapter(ArrayList<CustomerBookingDetails> orders,Context context, Callback callback)
    {
        this.orders=orders;
        this.context=context;
        this.callback=callback;
    }
    @Override
    public Object getDataAtPosition(int position) {
        return orders.get(position);
    }

    @Override
    public int getLayoutIdForType(int viewType) {
        return R.layout.previous_order_item_layout;
    }

    @Override
    public void onItemClick(Object object, int position) {
        callback.onEventDone(object);
    }

    @Override
    public void editHeightWidthItem(View view, ViewGroup parent) {
    }

    @Override
    public void onBindViewHolder(BaseAdapter.MyViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        View view=holder.itemView;
        TextView assisstance=view.findViewById(R.id.assisstance);
        assisstance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, BaseNavigationActivity.class);
                Bundle bundle=new Bundle();
                bundle.putString(FRAGMENT_NAME,CONTACT_US_FRAGMENT);
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }
}
