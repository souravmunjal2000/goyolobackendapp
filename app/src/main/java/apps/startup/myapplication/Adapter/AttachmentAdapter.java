package apps.startup.myapplication.Adapter;

import android.content.Context;
import android.telecom.Call;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import apps.startup.myapplication.Callback;
import apps.startup.myapplication.R;
import apps.startup.myapplication.base.BaseAdapter;

public class AttachmentAdapter extends BaseAdapter {
    ArrayList<String> urls;
    Context context;
    Callback callback;

    public AttachmentAdapter(ArrayList<String> urls,Context context,Callback callback)
    {
        this.callback=callback;
        this.urls=urls;
        this.context=context;
    }

    @Override
    public Object getDataAtPosition(int position) {
        return urls.get(position);
    }

    @Override
    public int getLayoutIdForType(int viewType) {
        return R.layout.attachment_items;
    }

    @Override
    public void onItemClick(Object object, int position) {
        callback.onEventDone(object);
    }

    @Override
    public void editHeightWidthItem(View view, ViewGroup parent) {

    }

    @Override
    public int getItemCount() {
        return urls.size();
    }
}
