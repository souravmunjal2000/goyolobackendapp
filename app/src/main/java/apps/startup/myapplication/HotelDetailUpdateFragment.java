package apps.startup.myapplication;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import apps.startup.myapplication.Adapter.FacilitiesAdapter;
import apps.startup.myapplication.application.SharedPrefHelper;
import apps.startup.myapplication.base.BaseFragment;
import apps.startup.myapplication.databinding.FragmentHotelDetailUpdateBinding;
import apps.startup.myapplication.model.AddShops;
import apps.startup.myapplication.model.Facilitiesandprices;

import static apps.startup.myapplication.Constants.HOTEL_DETIALS;
import static apps.startup.myapplication.Constants.SHOP_DATABASE_NAME;
import static apps.startup.myapplication.Constants.USER_ID;

public class HotelDetailUpdateFragment extends BaseFragment {
    AddShops hotelDetail;
    DatabaseReference databaseReference= FirebaseDatabase.getInstance().getReference().child(SHOP_DATABASE_NAME).child(SharedPrefHelper.getInstance().getStringData(USER_ID));
    FragmentHotelDetailUpdateBinding binding;
    @Override
    public int setViewId() {
        return R.layout.fragment_hotel_detail_update;
    }

    @Override
    public void onFragmentCreated() {
        hotelDetail=(AddShops) getActivity().getIntent().getExtras().getSerializable(HOTEL_DETIALS);
        databaseReference=databaseReference.child(hotelDetail.getName());
        FacilitiesAdapter facilitiesAdapter=new FacilitiesAdapter(hotelDetail.getWhat_shops_do(), new Callback() {
            @Override
            public void onEventDone(Object object) {

            }
        },getContext());
        binding.update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.progress.setVisibility(View.VISIBLE);
                update();
            }
        });
        binding.rvList.setAdapter(facilitiesAdapter);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        binding.rvList.setLayoutManager(linearLayoutManager);
    }

    private void update() {
        databaseReference.setValue(hotelDetail).addOnCompleteListener(getActivity(), new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                binding.progress.setVisibility(View.INVISIBLE);
                Toaster.makeToast("Updated Rooms");
                getActivity().finish();
            }
        });
    }

    @Override
    public void bindView(View view) {
        binding= DataBindingUtil.bind(view);
    }

    @Override
    public void getComponentFactory() {

    }
}
