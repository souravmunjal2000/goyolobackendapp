package apps.startup.myapplication;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;

import apps.startup.myapplication.LandingActivity;
import apps.startup.myapplication.LoginActivity;
import apps.startup.myapplication.R;
import apps.startup.myapplication.application.PrintApplication;
import apps.startup.myapplication.application.SharedPrefHelper;
import apps.startup.myapplication.base.BaseFragment;
import apps.startup.myapplication.databinding.FragmentHomeBinding;

import static apps.startup.myapplication.Constants.IS_LOGGED_IN;
import static apps.startup.myapplication.Constants.USER_ID;

public class HomeFragment extends BaseFragment {
    FragmentHomeBinding binding;
    @Override
    public int setViewId() {
        return R.layout.fragment_home;
    }

    @Override
    public void onFragmentCreated() {

        binding.logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPrefHelper.getInstance().setBooleanData(IS_LOGGED_IN,false);
                Intent intent=new Intent(getContext(),LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
    }

    @Override
    public void bindView(View view) {
        binding=DataBindingUtil.bind(view);
    }

    @Override
    public void getComponentFactory() {

    }
}
