package apps.startup.myapplication.application;

import android.app.Application;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;

import apps.startup.myapplication.Notifications.NotificationService;
import apps.startup.myapplication.R;
import apps.startup.myapplication.base.MyDataBindingComponent;
import io.fabric.sdk.android.Fabric;


/**
 * Created by prateek on 16/02/18.
 */
public class PrintApplication extends Application {

    private static PrintApplication instance = null;

    public static PrintApplication getInstance(){
        return instance;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        instance = this;
       // Fabric.with(this, new Crashlytics());
        DataBindingUtil.setDefaultComponent(new MyDataBindingComponent());
    }
}
