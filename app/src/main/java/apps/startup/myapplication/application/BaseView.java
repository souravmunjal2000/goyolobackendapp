package apps.startup.myapplication.application;

/**
 * Created by prateek on 13/06/18.
 */

public interface BaseView {
    void showToast(int resId);
    void showToast(String message);
}
