package apps.startup.myapplication;

/**
 * Created by prateek on 29/05/18.
 */

public interface Callback {
    void onEventDone(Object object);
}
