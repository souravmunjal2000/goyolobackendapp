package apps.startup.myapplication;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.concurrent.TimeUnit;

import apps.startup.myapplication.Utils.BasicUtils;
import apps.startup.myapplication.base.BaseDialogFragment;
import apps.startup.myapplication.databinding.DialogVerfiyOtpBinding;

public class VerifyOtpDialog extends BaseDialogFragment {
    DialogVerfiyOtpBinding binding;
    private FirebaseAuth mAuth;
    private String mVerificationId;
    private String phone_no;
    private String otp;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            otp = phoneAuthCredential.getSmsCode();
            if (otp != null) {
                binding.otp.setText(otp);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            binding.progressBar.setVisibility(View.INVISIBLE);
            mVerificationId = s;
        }
    };
    public static VerifyOtpDialog newInstance() {
        VerifyOtpDialog verifyOtpDialog = new VerifyOtpDialog();
        return verifyOtpDialog;
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final View dialogView = getActivity().getLayoutInflater()
                .inflate(R.layout.dialog_verfiy_otp, new LinearLayout(getActivity()),
                        false);
        binding= DataBindingUtil.bind(dialogView);
        final Dialog builder = new Dialog(getActivity());
        mAuth = FirebaseAuth.getInstance();

        binding.sendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(BasicUtils.isValidMobile(binding.phoneNo.getText().toString())) {
                    phone_no =binding.phoneNo.getText().toString();
                    sendVerificationCode(phone_no);
                }
                else
                {
                    binding.progressBar.setVisibility(View.VISIBLE);
                    binding.phoneNo.setError(getText(R.string.invalid_phone_error));
                }
            }
        });

        binding.verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, otp);
                if(credential.getSmsCode().equals(otp)) {
                    EventBus.getDefault().post("+91"+phone_no);
                    builder.dismiss();
                }
                else
                { }
            }
        });

        binding.cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                builder.dismiss();
            }
        });

        builder.setCancelable(false);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (builder.getWindow() != null) {
            builder.getWindow()
                    .setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }

        builder.setContentView(dialogView);
        return builder;
    }

    private void sendVerificationCode(String mobile) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                "+91"+mobile,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallbacks);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding.unbind();
    }

}
