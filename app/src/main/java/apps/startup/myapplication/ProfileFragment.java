package apps.startup.myapplication;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.RatingBar;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Objects;

import apps.startup.myapplication.application.SharedPrefHelper;
import apps.startup.myapplication.base.BaseFragment;
import apps.startup.myapplication.databinding.FragmentProfileBinding;
import apps.startup.myapplication.model.User;

import static apps.startup.myapplication.Constants.APP_URL;
import static apps.startup.myapplication.Constants.DATABASE_NAME;
import static apps.startup.myapplication.Constants.USER_ID;
import static apps.startup.myapplication.Constants.USER_NAME;

public class ProfileFragment extends BaseFragment {
    FragmentProfileBinding binding;
    String userid= SharedPrefHelper.getInstance().getStringData(USER_ID);
    String user_name=SharedPrefHelper.getInstance().getStringData(USER_NAME);
    @Override
    public int setViewId() {
        return R.layout.fragment_profile;
    }

    @Override
    public void onFragmentCreated() {
        binding.userId.setText(userid);
        binding.userName.setText(user_name);
    }




    @Override
    public void bindView(View view) {
        binding=DataBindingUtil.bind(view);
    }

    @Override
    public void getComponentFactory() {
    }

}
