package apps.startup.myapplication;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.view.View;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import apps.startup.myapplication.base.BaseFragment;
import apps.startup.myapplication.databinding.FragmentContactUsBinding;

import static apps.startup.myapplication.Constants.QUERY_DATABASE_NAME;

public class ContactUsFragment extends BaseFragment {
    FragmentContactUsBinding binding;
    DatabaseReference databaseReference= FirebaseDatabase.getInstance().getReference().child(QUERY_DATABASE_NAME);
    @Override
    public int setViewId() {
        return R.layout.fragment_contact_us;
    }

    @Override
    public void onFragmentCreated() {

        binding.send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(binding.message.getText().toString().equals("") || binding.emailAddress.getText().toString().equals("") || binding.name.getText().toString().equals("")){
                    Toaster.makeToast("Please fill the relevant fields");
                    return;
                }
                String message=binding.name.getText().toString()+" "+binding.emailAddress.getText().toString()+" "+binding.message.getText().toString();
                databaseReference.push().setValue(message).addOnCompleteListener(getActivity(), new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toaster.makeToast("Your query has been received");
                        getActivity().finish();
                    }
                });

            }
        });

    }

    @Override
    public void bindView(View view) {
    binding= DataBindingUtil.bind(view);
    }

    @Override
    public void getComponentFactory() {

    }
}
