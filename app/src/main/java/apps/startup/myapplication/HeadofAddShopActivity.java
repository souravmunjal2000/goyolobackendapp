package apps.startup.myapplication;

import android.Manifest;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import apps.startup.myapplication.Utils.PermissionUtils;
import apps.startup.myapplication.base.BaseActivity;

public class HeadofAddShopActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_head_add_shops);
        boolean CameraPermission=PermissionUtils.checkPermission(this,Manifest.permission.CAMERA);
        boolean LocationPermission=PermissionUtils.checkPermission(this,Manifest.permission.ACCESS_FINE_LOCATION);
        if(!CameraPermission && !LocationPermission)
        {
            PermissionUtils.askPermission(this,new String[]{Manifest.permission.CAMERA,Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION});
        }
    }

    @Override
    public boolean showBackButton() {
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == Constants.REQEST_PERMISSION_CODE) {
            boolean allPermissionsGranted = true;
            Log.e("tab",""+permissions.length+grantResults.length);
            for(int result : grantResults) {
                if(result != 0) {
                    allPermissionsGranted = false;
                }
            }

            if(!allPermissionsGranted) {
                onBackPressed();
            }else{
            }
        }
    }
}
