package apps.startup.myapplication.model;

import java.io.Serializable;

public class FacilitiesPricesNumber implements Serializable {
    String facilities;
    int prices;
    int numbers;
    int totalprice;

    public String getFacilities() {
        return facilities;
    }

    public void setFacilities(String facilities) {
        this.facilities = facilities;
    }

    public int getPrices() {
        return prices;
    }

    public void setPrices(int prices) {
        this.prices = prices;
    }

    public int getNumbers() {
        return numbers;
    }

    public void setNumbers(int numbers) {
        this.numbers = numbers;
    }

    public int getTotalprice() {
        return numbers*prices;
    }

}
