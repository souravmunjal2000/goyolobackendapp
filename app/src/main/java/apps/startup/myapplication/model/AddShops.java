package apps.startup.myapplication.model;

import java.io.Serializable;
import java.util.ArrayList;

public class AddShops implements Serializable {
    String name;
    ArrayList<Facilitiesandprices> what_shops_do;
    ArrayList<String> generalPhotoList;
    String phoneno;
    String cleaningStaffPhoneno1,cleaningStaffPhoneno2,securityGuardPhoneno;
    float rating;
    String description;
    String userid;
    String whatshopsdo;
    String photourl;
    String address;
    ArrayList<String> facilities;
    double lat=0,longi=0;
    String affiliatedCollege;
    String typeOfFood,typeOfPG;
    int securityDeposit;
    String basisOfPayment;
    int messCharges;

    public int getMessCharges() {
        return messCharges;
    }

    public void setMessCharges(int messCharges) {
        this.messCharges = messCharges;
    }

    public String getBasisOfPayment() {
        return basisOfPayment;
    }

    public void setBasisOfPayment(String basisOfPayment) {
        this.basisOfPayment = basisOfPayment;
    }

    public String getAffiliatedCollege() {
        return affiliatedCollege;
    }

    public void setAffiliatedCollege(String affiliatedCollege) {
        this.affiliatedCollege = affiliatedCollege;
    }

    public String getCleaningStaffPhoneno1() {
        return cleaningStaffPhoneno1;
    }

    public void setCleaningStaffPhoneno1(String cleaningStaffPhoneno1) {
        this.cleaningStaffPhoneno1 = cleaningStaffPhoneno1;
    }

    public String getCleaningStaffPhoneno2() {
        return cleaningStaffPhoneno2;
    }

    public void setCleaningStaffPhoneno2(String cleaningStaffPhoneno2) {
        this.cleaningStaffPhoneno2 = cleaningStaffPhoneno2;
    }

    public String getSecurityGuardPhoneno() {
        return securityGuardPhoneno;
    }

    public void setSecurityGuardPhoneno(String securityGuardPhoneno) {
        this.securityGuardPhoneno = securityGuardPhoneno;
    }

    public String getTypeOfFood() {
        return typeOfFood;
    }

    public void setTypeOfFood(String typeOfFood) {
        this.typeOfFood = typeOfFood;
    }

    public String getTypeOfPG() {
        return typeOfPG;
    }

    public void setTypeOfPG(String typeOfPG) {
        this.typeOfPG = typeOfPG;
    }

    public int getSecurityDeposit() {
        return securityDeposit;
    }

    public void setSecurityDeposit(int securityDeposit) {
        this.securityDeposit = securityDeposit;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<String> getFacilities() {
        return facilities;
    }

    public void setFacilities(ArrayList<String> facilities) {
        this.facilities = facilities;
    }

    public ArrayList<String> getGeneralPhotoList() {
        return generalPhotoList;
    }

    public void setGeneralPhotoList(ArrayList<String> generalPhotoList) {
        this.generalPhotoList = generalPhotoList;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLongi() {
        return longi;
    }

    public void setLongi(double longi) {
        this.longi = longi;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhotourl() {
        return photourl;
    }

    public void setPhotourl(String photourl) {
        this.photourl = photourl;
    }

    public String getWhatshopsdo() {
        return whatshopsdo;
    }

    public void setWhatshopsdo(String whatshopsdo) {
        this.whatshopsdo = whatshopsdo;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getPhoneno() {
        return phoneno;
    }

    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Facilitiesandprices> getWhat_shops_do() {
        return what_shops_do;
    }

    public void setWhat_shops_do(ArrayList<Facilitiesandprices> what_shops_do) {
        this.what_shops_do = what_shops_do;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }
}
