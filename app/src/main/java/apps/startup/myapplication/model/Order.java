package apps.startup.myapplication.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Order implements Serializable {
    ArrayList<FacilitiesPricesNumber> facilitiesPricesNumbers=new ArrayList<>();
    int totalPrice=0;
    AddShops shop;
    String dateandtime;
    ArrayList<String> attachmentsUrl=new ArrayList<>();
    String status="Processing";
    String keyOfUser;
    String documentViewingApp;
    String receivingMethod;
    String billPhoto;
    String billStatus;

    public String getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(String billStatus) {
        this.billStatus = billStatus;
    }


    public String getKeyOfUser() {
        return keyOfUser;
    }

    public void setKeyOfUser(String keyOfUser) {
        this.keyOfUser = keyOfUser;
    }

    public String getBillPhoto() {
        return billPhoto;
    }

    public void setBillPhoto(String billPhoto) {
        this.billPhoto = billPhoto;
    }

    public String getReceivingMethod() {
        return receivingMethod;
    }

    public void setReceivingMethod(String receivingType) {
        this.receivingMethod = receivingType;
    }

    public String getDocumentViewingApp() {
        return documentViewingApp;
    }

    public void setDocumentViewingApp(String documentViewingApp) {
        this.documentViewingApp = documentViewingApp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<String> getAttachmentsUrl() {
        return attachmentsUrl;
    }

    public void setAttachmentsUrl(ArrayList<String> attachmentsUrl) {
        this.attachmentsUrl = attachmentsUrl;
    }

    public String getDateandtime() {
        return dateandtime;
    }

    public void setDateandtime(String dateandtime) {
        this.dateandtime = dateandtime;
    }

    public AddShops getShop() {
        return shop;
    }

    public void setShop(AddShops shop) {
        this.shop = shop;
    }

    public ArrayList<FacilitiesPricesNumber> getFacilitiesPricesNumbers() {
        return facilitiesPricesNumbers;
    }

    public void setFacilitiesPricesNumbers(ArrayList<FacilitiesPricesNumber> facilitiesPricesNumbers) {
        this.facilitiesPricesNumbers = facilitiesPricesNumbers;
    }

    public void setTotalPrice(int totalPrice)
    {
        this.totalPrice=totalPrice;
    }

    public int getTotalPrice() {
//        for(FacilitiesPricesNumber facilitiesPricesNumber:facilitiesPricesNumbers)
//        {
//            totalPrice=totalPrice+(facilitiesPricesNumber.getNumbers()*facilitiesPricesNumber.getPrices());
//        }
        return totalPrice;
    }

}
