package apps.startup.myapplication.model;

import java.io.Serializable;

public class TotalBookedItems implements Serializable {
    String facilityName;
    int rooms;
    String pricesPerRoom;
    int duration;

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getPricesPerRoom() {
        return pricesPerRoom;
    }

    public void setPricesPerRoom(String pricesPerRoom) {
        this.pricesPerRoom = pricesPerRoom;
    }

    public String getFacilityName() {
        return facilityName;
    }

    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

    public int getRooms() {
        return rooms;
    }

    public void setRooms(int rooms) {
        this.rooms = rooms;
    }
}
