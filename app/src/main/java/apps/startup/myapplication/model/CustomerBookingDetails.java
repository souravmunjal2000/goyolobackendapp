package apps.startup.myapplication.model;

import java.io.Serializable;
import java.util.ArrayList;

public class CustomerBookingDetails implements Serializable {
    String customerName;
    String customerPhoneNo;
    String hotel,hotelOnwerUID,hotelPhotoUrl;
    String entryDate,exitDate;
    String bookingID;
    boolean hasPaidFull=false;
    ArrayList<TotalBookedItems> totalBookedItems=new ArrayList<>();
    int duration;
    int discount=0;

    public String getHotelOnwerUID() {
        return hotelOnwerUID;
    }

    public void setHotelOnwerUID(String hotelOnwerUID) {
        this.hotelOnwerUID = hotelOnwerUID;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getHotelPhotoUrl() {
        return hotelPhotoUrl;
    }

    public void setHotelPhotoUrl(String hotelPhotoUrl) {
        this.hotelPhotoUrl = hotelPhotoUrl;
    }

    public boolean getHasPaidFull() {
        return hasPaidFull;
    }

    public void setHasPaidFull(boolean hasPaidFull) {
        this.hasPaidFull = hasPaidFull;
    }

    public String getBookingID() {
        return bookingID;
    }

    public void setBookingID(String bookingID) {
        this.bookingID = bookingID;
    }

    public ArrayList<TotalBookedItems> getTotalBookedItems() {
        return totalBookedItems;
    }

    public void setTotalBookedItems(ArrayList<TotalBookedItems> totalBookedItems) {
        this.totalBookedItems = totalBookedItems;
    }

    public String getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(String entryDate) {
        this.entryDate = entryDate;
    }

    public String getExitDate() {
        return exitDate;
    }

    public void setExitDate(String exitDate) {
        this.exitDate = exitDate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhoneNo() {
        return customerPhoneNo;
    }

    public void setCustomerPhoneNo(String customerPhoneNo) {
        this.customerPhoneNo = customerPhoneNo;
    }

    public String getHotel() {
        return hotel;
    }

    public void setHotel(String hotel) {
        this.hotel = hotel;
    }

}
