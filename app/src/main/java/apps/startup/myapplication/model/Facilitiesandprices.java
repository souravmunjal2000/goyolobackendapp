package apps.startup.myapplication.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Facilitiesandprices implements Serializable {
    String prices;
    String facilities;
    int numberofrooms;
    ArrayList<String> images;


    public String getFacilities() {
        return facilities;
    }

    public void setFacilities(String facilities) {
        this.facilities = facilities;
    }

    public String getPrices() {
        return prices;
    }

    public void setPrices(String prices) {
        this.prices = prices;
    }


    public int getNumberofrooms() {
        return numberofrooms;
    }

    public void setNumberofrooms(int numberofrooms) {
        this.numberofrooms = numberofrooms;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }
}

