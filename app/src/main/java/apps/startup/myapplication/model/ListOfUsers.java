package apps.startup.myapplication.model;

import java.util.ArrayList;

public class ListOfUsers {
    ArrayList<User> userArrayList=new ArrayList<>();

    public ArrayList<User> getUserArrayList() {
        return userArrayList;
    }

    public void setUserArrayList(ArrayList<User> userArrayList) {
        this.userArrayList = userArrayList;
    }
}
