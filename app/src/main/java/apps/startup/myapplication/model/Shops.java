package apps.startup.myapplication.model;

import java.util.ArrayList;
import java.util.List;

public class Shops {
    String name;
    String what_shops_do;
    int rating;
    String photo;

    public Shops(String name, String what_shops_do, int rating, String photo) {
        this.name = name;
        this.what_shops_do = what_shops_do;
        this.rating = rating;
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWhat_shops_do() {
        return what_shops_do;
    }

    public void setWhat_shops_do(String what_shops_do) {
        this.what_shops_do = what_shops_do;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

}
