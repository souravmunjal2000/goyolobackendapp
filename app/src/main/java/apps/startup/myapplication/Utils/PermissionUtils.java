package apps.startup.myapplication.Utils;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import apps.startup.myapplication.Constants;

public class PermissionUtils {
    /**
     * This function is contains all the permission if you any of the permission just call this function
     * @param activity It takes activity as instance
     */
    public static void askPermissionInStart(Activity activity) {
        String permission[]=new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA};
        ActivityCompat.requestPermissions(activity,
                permission,
                Constants.REQEST_PERMISSION_CODE);
    }

    /**
     * This function check if the permission is granted or not
     * @param activity Activity on which you are working
     * @param permission It is the permission
     * @return
     */
    public static boolean checkPermission(Activity activity,String permission) {
        if(ContextCompat.checkSelfPermission(activity,permission)!=PackageManager.PERMISSION_GRANTED)
            return false;
        else
            return true;
    }

    /**
     * This Function allows you to ask multiple permission
     * @param activity Activity you are on
     * @param permission The array of string (permission)
     */
    public static void askPermission(Activity activity,String[] permission){
        ActivityCompat.requestPermissions(activity,
                permission,
                Constants.REQEST_PERMISSION_CODE);
    }

}
