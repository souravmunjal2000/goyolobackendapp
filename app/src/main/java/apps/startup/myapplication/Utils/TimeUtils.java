package apps.startup.myapplication.Utils;

import android.text.format.DateFormat;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class TimeUtils {
    public static String getDate(String timeStamp){
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        try{
        cal.setTimeInMillis(Long.parseLong(timeStamp) * 1000);
        String date = DateFormat.format("dd-MM-yyyy", cal).toString();
        return date;
        }
        catch (Exception e)
        {
            return "xx";
        }
    }
}
