package apps.startup.myapplication.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.support.v4.content.FileProvider;
import android.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import apps.startup.myapplication.customcamera.ImageUri;


public class ImageUtils{
        public static ImageUri getBitmapFile(Uri image, Context context) throws IOException {
            final int THUMBSIZE = 256;
            Log.e("the image is ","the big image path is "+image.getPath());
            Bitmap ThumbImage = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeStream(context.getContentResolver().openInputStream(image)),
                    THUMBSIZE, THUMBSIZE);
            File f=new FileUtils().getImagefile(context);
            FileOutputStream fout=new FileOutputStream(f);
            ThumbImage.compress(Bitmap.CompressFormat.JPEG, 100, fout);
            ImageUri imageUri=new ImageUri();
            imageUri.setUri(FileProvider.getUriForFile(context,
                    "com.startup.print",
                    f));
            return imageUri;
        }
}
